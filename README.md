# marker_detect
marker_detect is a package for the Robot Operating System [ROS](http://www.ros.org/) that integrates the [ARUCO](http://www.uco.es/investiga/grupos/ava/node/26) marker detection software into ROS. It is based on [ros_aruco](https://github.com/warp1337/ros_aruco).

# Features
* Marker detection for up to 1024 markers that are detected and published
* publishes full transformation matrix for easy use with rospy and numpy
* publishes debug image with markers and their orientation
* publishes marker pose as geometry_mesg
* input images can be read from a ros topic, a video file or a webcam

# TODOs
* debug image is only available if the input is a ros topic. cv::bridge needs aditional information to convert webcam image to ros topic.
* debug image is only published as raw type, image_transport should be used to compress data.

# Installation

##download aruco-1.2.5.tgz
```
#!bash
tar -xaf aruco-1.2.5.tgz
cd aruco-1.2.5
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=<arucodir> 
make
make install
```
##create catkin wokspace and install marker_detect in it
```
#!bash
mkdir catkinws
cd catkinws
catkin_init_workspace
source devel/setup.bash
catkin_make
cd src
git clone git@bitbucket.org:NiklasGeorg/markerdetection.git
cd ..
catkin_make --pkg marker_detect -DARUCO_PATH=<arucodir> -Daruco_DIR=<arucodownloaddir>/aruco-1.2.5/build
```
##calibrate camera
download opencvsamples
```
#!bash
g++ `pkg-config --cflags opencv` -o calibration  calibration.cpp `pkg-config --libs opencv`
./calibration -w <chessboard-columns> -h <chessboard-rows> -s <chessboard_size of squares> -o <calibrationfile>.yml -op [<videodevice-id>]
```
##start detection
```
#!bash
cd catkinws
devel/lib/marker_detect/marker_detect topic:</ros/toic>|live[:<camID>]|<video.avi> <calibrationfile>.yml <markersize_mm>
rostopic echo /markers/pose
rosrun image_view image_view image:=</markers/image>
```
This should show you the information for all markers and the original image with added coordinate systems for each marker that got detected. The image feed is currently only implemented for images from rostopics and not from videos or webcams.