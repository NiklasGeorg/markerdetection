/*****************************************************************************************

Copyright 2011 Rafael Muñoz Salinas. All rights reserved.
ROS bits and integration added by Florian Lier flier at techfak dot uni-bielefeld dot de

Redistribution and use in source and binary forms, with or without modification, are
permitted provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above copyright notice, this list
      of conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY Rafael Muñoz Salinas ''AS IS'' AND ANY EXPRESS OR IMPLIED
etARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Rafael Muñoz Salinas OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the
authors and should not be interpreted as representing official policies, either expressed
or implied, of Rafael Muñoz Salinas.
********************************************************************************************/
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <unistd.h>
#include "aruco.h"
#include "cvdrawingutils.h"
#include "ros/ros.h"
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/Pose.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/opencv.hpp>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <marker_detect/rosMarker.h>
#include <marker_detect/rosMarkers.h>

using namespace marker_detect;
using namespace cv;
using namespace aruco;

string TheInputVideo;
string TheIntrinsicFile;
float TheMarkerSize=-1;
int ThePyrDownLevel;
MarkerDetector MDetector;
CameraParameters TheCameraParameters;
VideoCapture TheVideoCapturer;
bool readCameraParameters(string TheIntrinsicFile,CameraParameters &CP,Size size);
// Determines the average time required for detection
pair<double,double> AvrgTime(0,0) ;
double ThresParam1,ThresParam2;
//tf::TransformBroadcaster* broadcaster;
// Publish pose message and buffer up to 100 messages
ros::Publisher* pose_pub; 
//Use method of ImageTransport to create image publisher
//image_transport::Publisher* img_pub;
ros::Publisher* img_pub;
Mat TheInputImage;

//Store all constants for image encodings in the enc namespace to be used later.
namespace enc = sensor_msgs::image_encodings;

void processImage()
{
	vector<Marker> TheMarkers;
	//ros::spinOnce();
	
	cout << "Detecting Markers" << endl;
	//index++; // Number of images captured
	double tick = (double)getTickCount();// For checking the speed

	// Detection of markers in the image passed
	MDetector.detect(TheInputImage,TheMarkers,TheCameraParameters,TheMarkerSize);

	// Check the speed by calculating the mean speed of all iterations
	AvrgTime.first+=((double)getTickCount()-tick)/getTickFrequency();
	AvrgTime.second++;

	// Show the detection time
	 cout<<"Time detection="<<1000*AvrgTime.first/AvrgTime.second<<" milliseconds"<<endl;

	// Collect the Info for the detected Markers and publish it
	rosMarkers msg;

	float x_t, y_t, z_t;
	float roll,yaw,pitch;
	// Marker rotation should be initially zero (just for convenience)
	float p_off = CV_PI;
	float r_off = CV_PI/2;
	float y_off = CV_PI/2;

	for (int i=0; i< TheMarkers.size(); i++) {
		// extract data
		x_t = -TheMarkers[i].Tvec.at<Vec3f>(0,0)[0];
		y_t = TheMarkers[i].Tvec.at<Vec3f>(0,0)[1];
		z_t = TheMarkers[i].Tvec.at<Vec3f>(0,0)[2];

		cv::Mat rot_mat(3,3,cv::DataType<float>::type);
		// You need to apply cv::Rodrigues() in order to obatain angles wrt to camera coords
		cv::Rodrigues(TheMarkers[i].Rvec,rot_mat);

		// convert
		pitch   = -atan2(rot_mat.at<float>(2,0), rot_mat.at<float>(2,1));
		yaw     = acos(rot_mat.at<float>(2,2));
		roll    = -atan2(rot_mat.at<float>(0,2), rot_mat.at<float>(1,2));

		// See: http://en.wikipedia.org/wiki/Flight_dynamics
		printf( "Angles (deg) wrt Flight Dynamics: roll:%5.2f pitch:%5.2f yaw:%5.2f \n", (roll-r_off)*(180.0/CV_PI), (pitch-p_off)*(180.0/CV_PI), (yaw-y_off)*(180.0/CV_PI));
		printf( "       Marker distance in metres:  x_d:%5.2f   y_d:%5.2f z_d:%5.2f \n", x_t, y_t, z_t);

		// Now publish the pose message, remember the offsets
		geometry_msgs::Pose p;
		p.position.x = x_t;
		p.position.y = y_t;
		p.position.z = z_t;
		geometry_msgs::Quaternion p_quat = tf::createQuaternionMsgFromRollPitchYaw(roll-r_off, pitch+p_off, yaw-y_off);
		p.orientation = p_quat;
		rosMarker m;
	 	m.pose=p;
		m.id=TheMarkers[i].id;
		// Marker Points
		for (int j=0; j< TheMarkers[i].size(); j++) {
			geometry_msgs::Point point;
			point.x = TheMarkers[i][j].x;
			point.y = TheMarkers[i][j].y;
			point.z = 0;
			m.points.push_back(point);
		}
		// return transformation matrix
		for (int mati = 0; mati < 3; mati++) {
			for (int matj = 0; matj < 3; matj++) {
				m.matrix.push_back(rot_mat.at<float>(mati,matj));
			}
			if (mati == 0)
				m.matrix.push_back(-TheMarkers[i].Tvec.at<Vec3f>(0,0)[mati]);
			else
				m.matrix.push_back(TheMarkers[i].Tvec.at<Vec3f>(0,0)[mati]);
		}
		m.matrix.push_back(0.0f);
		m.matrix.push_back(0.0f);
		m.matrix.push_back(0.0f);
		m.matrix.push_back(1.0f);
		msg.markers.push_back(m);
	}
	pose_pub->publish(msg);
	ros::spinOnce();

	//// Publish TF message including the offsets
	//tf::Quaternion quat = tf::createQuaternionFromRPY(roll-p_off, pitch+p_off, yaw-y_off);
	//broadcaster->sendTransform(tf::StampedTransform(tf::Transform(quat, tf::Vector3(x_t, y_t, z_t)), ros::Time::now(),"camera", "marker"));

	// Draw a 3d cube in each marker if there is 3d info
	if (TheCameraParameters.isValid()){
		for (unsigned int i=0;i<TheMarkers.size();i++) {
			CvDrawingUtils::draw3dCube(TheInputImage,TheMarkers[i],TheCameraParameters);
			CvDrawingUtils::draw3dAxis(TheInputImage,TheMarkers[i],TheCameraParameters);
		}
	}
}

//This function is called everytime a new image is published
void imageCallback(const sensor_msgs::ImageConstPtr& original_image)
{
	//Convert from the ROS image message to a CvImage suitable for working with OpenCV for processing
	cout << "Converting Image" << endl;	
	cv_bridge::CvImagePtr rosInputImage;
	try
	{
		//Always copy, returning a mutable CvImage
		//OpenCV expects color images to use BGR channel order.
		rosInputImage = cv_bridge::toCvCopy(original_image, enc::BGR8);
	}
	catch (cv_bridge::Exception& e)
	{
		//if there is an error during conversion, display it
		ROS_ERROR("tutorialROSOpenCV::main.cpp::cv_bridge exception: %s", e.what());
		return;
	}
	TheInputImage = rosInputImage->image;
	processImage();	

	cout << "Output debugging image with detected markers" << endl;
	/**
	* The publish() function is how you send messages. The parameter
	* is the message object. The type of this object must agree with the type
	* given as a template parameter to the advertise<>() call, as was done
	* in the constructor in main().
	*/
	//Convert the CvImage to a ROS image message and publish it on the "camera/image_processed" topic.
  img_pub->publish(rosInputImage->toImageMsg());
}

bool readArguments ( int argc,char **argv )
{
	if (argc<2) {
		cerr<< "Invalid number of arguments" <<endl;
		cerr<< "Usage: (in.avi|live[:<camera-index>]|topic:<topic>) [intrinsics.yml] [size]" <<endl;
		return false;
	}
	TheInputVideo=argv[1];
	if (argc>=3)
		TheIntrinsicFile=argv[2];
	if (argc>=4)
		TheMarkerSize=atof(argv[3]);
	else
		cerr<< "NOTE: You need makersize to see 3d info!" <<endl;
	return true;
}

int main(int argc,char **argv){
	cout << "Read Arguments" << endl;

	if (readArguments(argc,argv)==false) {
		return 0;
	}
	cout << "Reading Camera Calibration" << endl;
	// Read camera parameters if passed
	if (TheIntrinsicFile!="") {
		TheCameraParameters.readFromXMLFile(TheIntrinsicFile);
	//		TheCameraParameters.resize(TheInputImage.size());
	cout << "Valid CameraParameters? " <<	TheCameraParameters.isValid() << endl;
}

	cout << "Configuring Detector" << endl;
	// Configure other parameters
	if (ThePyrDownLevel>0)
		MDetector.pyrDown(ThePyrDownLevel);
	MDetector.getThresholdParams( ThresParam1,ThresParam2);
	MDetector.setCornerRefinementMethod(MarkerDetector::LINES);

	cout << "Initializing ROS" << endl;
	// ROS messaging init
	/**
	* The ros::init() function needs to see argc and argv so that it can perform
	* any ROS arguments and name remapping that were provided at the command line. For programmatic
	* remappings you can use a different version of init() which takes remappings
	* directly, but for most command-line programs, passing argc and argv is the easiest
	* way to do it.  The third argument to init() is the name of the node. Node names must be unique in a running system.
	* The name used here must be a base name, ie. it cannot have a / in it.
	* You must call one of the versions of ros::init() before using any other
	* part of the ROS system.
	*/
	ros::init(argc, argv, "marker_detect_publisher");

	cout << "Creating Node Handle" << endl;
	/**
	* NodeHandle is the main access point to communications with the ROS system.
	* The first NodeHandle constructed will fully initialize this node, and the last
	* NodeHandle destructed will close down the node.
	*/
	ros::NodeHandle nh;

	//Create an ImageTransport instance, initializing it with our NodeHandle.
	cout << "Image Transport" << endl;
	image_transport::ImageTransport it(nh);

	cout << "Initializing Publishers" << endl;
	/**
	* The advertise() function is how you tell ROS that you want to
	* publish on a given topic name. This invokes a call to the ROS
	* master node, which keeps a registry of who is publishing and who
	* is subscribing. After this advertise() call is made, the master
	* node will notify anyone who is trying to subscribe to this topic name,
	* and they will in turn negotiate a peer-to-peer connection with this
	* node.  advertise() returns a Publisher object which allows you to
	* publish messages on that topic through a call to publish().  Once
	* all copies of the returned Publisher object are destroyed, the topic
	* will be automatically unadvertised.
	*
	* The second parameter to advertise() is the size of the message queue
	* used for publishing messages.  If messages are published more quickly
	* than we can send them, the number here specifies how many messages to
	* buffer up before throwing some away.
	*/
	img_pub = new ros::Publisher();
	*img_pub = nh.advertise<sensor_msgs::Image>("markers/image", 1);
	cout << "Initialised Image-Publisher; intializing Position-Publisher" << endl;
	pose_pub = new ros::Publisher();
	*pose_pub = nh.advertise<rosMarkers>("markers/poses", 1);
	cout << "initialised Publishers" << endl; //initialize Broadcaster" << endl;
	//broadcaster = new tf::TransformBroadcaster();

	// Select Video Source
	if (TheInputVideo.find("topic:") != string::npos) {
		// remove topic prefix
		TheInputVideo.erase(0,6);
		cout << "Subscribing to Image topic: " << TheInputVideo << endl;
		/**
		* Subscribe to the "camera/image_raw" base topic. The actual ROS topic subscribed to depends on which transport is used.
		* In the default case, "raw" transport, the topic is in fact "camera/image_raw" with type sensor_msgs/Image. ROS will call
		* the "imageCallback" function whenever a new image arrives. The 2nd argument is the queue size.
		* subscribe() returns an image_transport::Subscriber object, that you must hold on to until you want to unsubscribe.
		* When the Subscriber object is destructed, it will automatically unsubscribe from the "camera/image_raw" base topic.
		*/
		image_transport::Subscriber sub = it.subscribe(TheInputVideo, 1, imageCallback);
		/**
		* In this application all user callbacks will be called from within the ros::spin() call.
		* ros::spin() will not return until the node has been shutdown, either through a call
		* to ros::shutdown() or a Ctrl-C.
		*/
		cout << "Running ROS main loop" << endl;
		ros::spin();
	}
	//read from camera or from  file
	else {
		if (TheInputVideo.find("live")!=string::npos) {
			int vIdx=0;
			//check if the :idx is here
			char cad[100];
			if (TheInputVideo.find(":")!=string::npos) {
				std::replace(TheInputVideo.begin(),TheInputVideo.end(),':',' ');
				sscanf(TheInputVideo.c_str(),"%s %d",cad,&vIdx);
			}
			cout<<"Opening camera index "<<vIdx<<endl;
			TheVideoCapturer.open(vIdx);
		}
		else  TheVideoCapturer.open(TheInputVideo);

		//check video is open
		if (!TheVideoCapturer.isOpened()) {
			cerr<<"Could not open video"<<endl;
			return -1;
		}
		do {
				TheVideoCapturer.retrieve( TheInputImage);
				processImage();
				ros::spinOnce();
		}while(ros::ok() && TheVideoCapturer.grab());
	}

	//delete broadcaster;
	delete pose_pub;
	delete img_pub;
	//ROS_INFO is the replacement for printf/cout.
	ROS_INFO("tutorialROSOpenCV::main.cpp::No error.");
}
